# This file is part of open-fpsz.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
extends Node3D

@export_category("Types")
@export var SINGLEPLAYER : PackedScene
@export var MULTIPLAYER : PackedScene

@onready var boot_menu : BootMenu = $BootMenu

func _ready() -> void:
	# run server only in headless
	if DisplayServer.get_name() == "headless":
		Game.type = MULTIPLAYER.instantiate()
		Game.type.start_server(9000, MapsManager.maps[randi_range(0, len(MapsManager.maps) - 1)])
		return
	Game.exit_pressed.connect(_on_game_exit_pressed)
	boot_menu.start_demo.connect(_start_demo)
	boot_menu.multiplayer_panel.start_server.connect(_start_server)
	boot_menu.multiplayer_panel.join_server.connect(_join_server)

func _on_game_exit_pressed() -> void:
	Input.mouse_mode = Input.MOUSE_MODE_VISIBLE
	boot_menu.show()

func _start_demo() -> void:
	Game.type = SINGLEPLAYER.instantiate()
	Input.mouse_mode = Input.MOUSE_MODE_CAPTURED
	boot_menu.hide()

func _start_server(port: int, map: PackedScene, mode: Multiplayer.Mode, username: String) -> void:
	Game.type = MULTIPLAYER.instantiate()
	Game.type.start_server(port, map, mode, username)
	Input.mouse_mode = Input.MOUSE_MODE_CAPTURED
	boot_menu.hide()

func _join_server(host : String, port : int, username : String) -> void:
	Game.type = MULTIPLAYER.instantiate()
	if not Game.type.multiplayer.connected_to_server.is_connected(_on_connected_to_server):
		Game.type.multiplayer.connected_to_server.connect(_on_connected_to_server)
	if not multiplayer.connection_failed.is_connected(boot_menu.multiplayer_panel.modal.hide):
		multiplayer.connection_failed.connect(boot_menu.multiplayer_panel.modal.hide)
	Game.type.join_server(host, port, username)
	Input.mouse_mode = Input.MOUSE_MODE_CAPTURED

func _on_connected_to_server() -> void:
	boot_menu.multiplayer_panel.modal.hide()
	boot_menu.hide()
