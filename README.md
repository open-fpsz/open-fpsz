# open-fpsz

![open-fpsz banner](assets/banner.webp "A picture of a puppy with a `Shazbot` on top of its head asking the viewer to change the game, made by people like 🫵")

We love fpsz games, but we're tired of the genre being abandoned by prominent studios. To properly honor its legacy, we've chosen to develop our own game inspired by it, aiming to surpass its qualities with familiar jetpack and skiing mechanics.

## Prerequisities

Download the `Godot Engine` at https://godotengine.org/download

## Getting started

1. Clone the repository and open the`Godot Engine`
2. Navigate to the repository and import the `project.godot`
3. Start coding or run the project!

## Contributing

To get started contributing to the project, please review our [Contribution Guidelines](CONTRIBUTING.md). These guidelines outline best practices, coding standards, and other important informations to ensure that your contributions align with the project's goals and maintain consistency across the codebase.

We welcome contributions from the community, feel free to submit pull requests, report bugs, or share ideas for new features.

## Community

The best way to get in touch with everyone is to join us on our [Matrix](https://matrix.to/#/#open-fpsz:matrix.org) or our [Discord](https://discord.gg/tdmV3MxCn5).

## License

This project is licensed under the [GPLv3](https://www.gnu.org/licenses/gpl-3.0.html). For more details, please refer to the [LICENSE](LICENSE.md) file.

### Addons

Please note that addons are subject to their own separate license and may have different terms. Be sure to check each addon under [addons](addons) for its respective licenses.

