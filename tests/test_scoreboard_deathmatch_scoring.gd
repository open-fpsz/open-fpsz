# This file is part of open-fpsz.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
extends GutTest

#var PLAYER : PackedScene = preload("res://entities/player/player.tscn")
#var SCOREBOARD : PackedScene = preload("res://interfaces/scoreboard/scoreboard.tscn")
#
#var _subject : DeathmatchScoringComponent
#var _scoreboard : Object
#var _victim : Player
#var _killer : Player

#func _setup_player(player_id : int) -> Player:
	#var player : Player = PLAYER.instantiate()
	#player.name = str(player_id)
	#add_child(player) # we can't autofree in this test setup
	#var participant : MatchParticipant = player.match_participant
	#player.match_participant.player_id = player_id
	#_scoreboard.add_participant(participant)
	#_subject.subscribe_player(player)
	#_subject._players = self
	#return player
#
#func before_each() -> void:
	#_subject = DeathmatchScoringComponent.new()
	#_scoreboard = double(SCOREBOARD).instantiate()
	#stub(_scoreboard, 'add_score_to_player')
	#stub(_scoreboard, 'increment_kill_count')
	#_subject._scoreboard = _scoreboard
	#_victim = _setup_player(0)
	#_killer = _setup_player(1)
#
#func after_each() -> void:
	#_subject.unsubscribe_player(_victim)
	#_subject.unsubscribe_player(_killer)
	#_victim.free()
	#_killer.free()
	#_subject.free()
#
#func _kill_player(victim : Player, killer : Player) -> void:
	#victim.died.emit(victim, killer.match_participant.player_id)
#
#func test_player_gets_no_score_for_self_kill() -> void:
	#_kill_player(_victim, _victim)
	#assert_not_called(_scoreboard, 'add_score_to_player')
	#assert_not_called(_scoreboard, 'increment_kill_count')
#
#func test_killer_gets_score_after_kill() -> void:
	#_kill_player(_victim, _killer)
	#assert_called(_scoreboard, 'add_score_to_player', [_killer.match_participant, _subject.ON_KILL_SCORE])
	#assert_called(_scoreboard, 'increment_kill_count', [_killer.match_participant])
#
#func test_killer_leaves_before_kill() -> void:
	#remove_child(_killer)
	#_kill_player(_victim, _killer)
	#assert_not_called(_scoreboard, 'add_score_to_player')
	#assert_not_called(_scoreboard, 'increment_kill_count')
