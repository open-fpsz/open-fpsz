# This file is part of open-fpsz.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
extends GutTest

const SCOREBOARD : PackedScene = preload("res://interfaces/scoreboard/scoreboard.tscn")

var _teams : Teams
var _scoreboard : Scoreboard

func before_each() -> void:
	_teams = Teams.new()
	_scoreboard = SCOREBOARD.instantiate()
	autofree(_teams)
	add_child_autoqfree(_scoreboard)
	_teams.team_added.connect(_on_team_added)
	_teams.team_erased.connect(_on_team_erased)
	watch_signals(_teams)
	watch_signals(_scoreboard)

func _on_team_added(team_name: String) -> void:
	var panel : ScorePanel = _scoreboard.add_panel(team_name)
	var team : Team = _teams[team_name]
	team.renamed.connect(panel.title.set_text)
	team.player_added.connect(_on_team_player_added)
	team.player_erased.connect(_on_team_player_erased)

func _on_team_player_added(team_name: String, peer_id: int, username: String) -> void:
	var panel : ScorePanel = _scoreboard.panels.get_node(team_name)
	panel.add_entry(peer_id, username)
	
func _on_team_player_erased(team_name: String, peer_id: int) -> void:
	var panel : ScorePanel = _scoreboard.panels.get_node(team_name)
	panel.remove_entry_by_peer_id(peer_id)

func _on_team_erased(team_name: String) -> void:
	var panel : ScorePanel = _scoreboard.panels.get_node(team_name)
	_scoreboard.remove_panel(panel)

func test_size() -> void:
	assert_eq(_teams.size(), 0)
	assert_eq(_scoreboard.size(), 0)

func test_set_team() -> void:
	var team_name : String = "team0"
	_teams[team_name] = Team.new(team_name)
	assert_signal_emitted(_teams, "team_added")
	assert_eq(_teams.size(), 1)
	assert_eq(_scoreboard.size(), 1)
	
func test_get_team() -> void:
	test_set_team()
	var team0 : Team = _teams["team0"]
	assert_not_null(team0)

func test_add_team() -> void:
	_teams.add_team("team0")
	assert_signal_emitted(_teams, "team_added")
	assert_eq(_teams.size(), 1)
	assert_eq(_scoreboard.size(), 1)

func test_erase_team() -> void:
	test_add_team()
	_teams.erase("team0")
	assert_signal_emitted(_teams, "team_erased")
	assert_eq(_teams.size(), 0)
	assert_eq(_scoreboard.size(), 0)
	
func test_team_signals() -> void:
	test_add_team()
	var team : Team = _teams["team0"]
	watch_signals(team)
	var player : Player = Player.new()
	team.add(player.peer_id, player.username)
	assert_signal_emitted(team, "player_added")
	assert_eq(team.size(), 1)
	assert_eq(_scoreboard.get_panel(0).size(), 1)
	team.erase(player.peer_id)
	assert_signal_emitted(team, "player_erased")
	assert_eq(team.size(), 0)
	assert_eq(_scoreboard.get_panel(0).size(), 0)
	player.queue_free()
