# This file is part of open-fpsz.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
extends GutTest

const FLAG : PackedScene = preload("res://entities/flag/flag.tscn")

var _subject : Flag
var _carry : FlagCarryComponent

func before_each() -> void:
	_subject = FLAG.instantiate()
	_carry = FlagCarryComponent.new()
	watch_signals(_subject)
	add_child_autofree(_subject)
	add_child_autofree(_carry)

func test_flag_default_state() -> void:
	assert_eq(_subject.state, Flag.FlagState.ON_STAND)

func test_flag_default_visibility() -> void:
	assert_true(_subject.is_visible())

func test_grab_flag() -> void:
	_carry.grab(_subject)
	assert_signal_emitted_with_parameters(_subject, 'grabbed', [_carry])
	assert_eq(_subject.state, Flag.FlagState.TAKEN)
	assert_eq(_subject.last_carrier, null)
	assert_false(_subject.is_visible())
	
func test_drop_flag() -> void:
	test_grab_flag()
	_carry.drop()
	assert_signal_emitted_with_parameters(_subject, 'dropped', [_carry])
	assert_eq(_subject.state, Flag.FlagState.DROPPED)
	assert_eq(_subject.last_carrier, _carry)
	assert_true(_subject.is_visible())

func test_that_flag_grab_hides_flag() -> void:
	test_grab_flag()
	assert_false(_subject.is_visible())

func test_grab_taken_flag() -> void:
	test_grab_flag()
	var _new_carry : FlagCarryComponent = FlagCarryComponent.new()
	add_child_autofree(_new_carry)
	_new_carry.grab(_subject)
	assert_null(_new_carry._flag)
	assert_eq(_carry._flag, _subject)
