# This file is part of open-fpsz.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
extends GutTest

var _subject:Health
const TEST_MAX_VALUE:int = 255

func before_each() -> void:
	_subject = Health.new()
	watch_signals(_subject)
	autoqfree(_subject)

func test_that_it_has_max_health_when_ready() -> void:
	assert_eq(_subject.value, _subject.max_value)

func test_that_it_takes_damage_from_opponent_team() -> void:
	var amount:int = 42
	_subject.damage(amount, -1)
	assert_eq(_subject.value, TEST_MAX_VALUE - amount)

func test_that_it_can_self_damage() -> void:
	var amount:int = 42
	_subject.damage(amount, 1)
	assert_eq(_subject.value, TEST_MAX_VALUE - amount)

func test_that_it_emits_health_changed_after_damage() -> void:
	_subject.damage(42, -1)
	assert_signal_emitted(_subject, 'updated')

func test_that_it_emits_signals_when_health_value_is_min_value() -> void:
	_subject.damage(TEST_MAX_VALUE, -1)
	assert_signal_emitted_with_parameters(_subject, 'killed', [-1])
	assert_signal_emitted(_subject, 'exhausted')

func test_that_it_heals_fully() -> void:
	_subject.value = 42
	_subject.heal()
	assert_eq(_subject.value, TEST_MAX_VALUE)
