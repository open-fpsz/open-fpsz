# This file is part of open-fpsz.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
extends GutTest

const SCOREBOARD : PackedScene = preload("res://interfaces/scoreboard/scoreboard.tscn")

var _scoreboard : Scoreboard

func before_each() -> void:
	_scoreboard = SCOREBOARD.instantiate()
	add_child_autoqfree(_scoreboard)

func test_size() -> void:
	assert_eq(_scoreboard.size(), 0)

# panels

func test_add_panel() -> void:
	_scoreboard.add_panel()
	assert_eq(_scoreboard.size(), 1)

func test_remove_panel() -> void:
	var panel : ScorePanel = _scoreboard.add_panel()
	_scoreboard.remove_panel(panel)
	assert_eq(_scoreboard.size(), 0)

func test_get_panel() -> void:
	_scoreboard.add_panel()
	var panel : ScorePanel = _scoreboard.get_panel(0)
	assert_not_null(panel)

# entries

func test_panel_add_entry() -> void:
	var panel : ScorePanel = _scoreboard.add_panel()
	var entry : ScorePanelEntry = panel.add_entry(1, "mercury")
	assert_not_null(entry)
	assert_eq(panel.size(), 1)

func test_panel_remove_entry() -> void:
	var panel : ScorePanel = _scoreboard.add_panel()
	var entry : ScorePanelEntry = panel.add_entry(1, "mercury")
	assert_eq(panel.size(), 1)
	panel.remove_entry(entry)
	assert_eq(panel.size(), 0)

func test_panel_remove_entry_by_name() -> void:
	var panel : ScorePanel = _scoreboard.add_panel()
	var entry : ScorePanelEntry = panel.add_entry(1, "mercury")
	assert_eq(panel.size(), 1)
	assert_true(panel.remove_entry_by_name(entry.name))
	assert_eq(panel.size(), 0)

func test_panel_remove_entry_by_peer_id() -> void:
	var panel : ScorePanel = _scoreboard.add_panel()
	panel.add_entry(1, "mercury")
	assert_eq(panel.size(), 1)
	assert_true(panel.remove_entry_by_peer_id(1))
	assert_eq(panel.size(), 0)
	
func test_panel_get_entry() -> void:
	var panel : ScorePanel = _scoreboard.add_panel()
	panel.add_entry(1, "mercury")
	var entry : ScorePanelEntry = panel.get_entry(0)
	assert_not_null(entry)
	assert_eq(entry.name, "1")
