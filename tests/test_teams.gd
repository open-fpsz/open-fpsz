# This file is part of open-fpsz.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
extends GutTest

var _teams : Teams

func before_each() -> void:
	_teams = Teams.new()
	autofree(_teams)
	
func test_size() -> void:
	assert_eq(_teams.size(), 0)

func test_set_team() -> void:
	var team_name : String = "team0"
	_teams[team_name] = Team.new(team_name)
	assert_eq(_teams.size(), 1)
	
func test_get_team() -> void:
	test_set_team()
	var team0 : Team = _teams["team0"]
	assert_not_null(team0)

func test_add_team() -> void:
	_teams.add_team("team0")
	assert_eq(_teams.size(), 1)

func test_erase_team() -> void:
	test_add_team()
	_teams.erase("team0")
	assert_eq(_teams.size(), 0)
	
func test_add_teams() -> void:
	_teams.add_teams(["team0", "team1"])
	assert_eq(_teams.size(), 2)
	
# Team

func test_team_size() -> void:
	test_add_team()
	var team : Team = _teams["team0"]
	assert_eq(team.size(), 0)
	
func test_team_signals() -> void:
	test_add_team()
	var team : Team = _teams["team0"]
	watch_signals(team)
	var player : Player = Player.new()
	team.add(player.peer_id, player.username)
	assert_signal_emitted(team, "player_added")
	assert_eq(team.size(), 1)
	team.erase(player.peer_id)
	assert_signal_emitted(team, "player_erased")
	assert_eq(team.size(), 0)
	player.free()
