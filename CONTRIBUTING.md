[[_TOC_]]

# How to Contribute?

Thank you for considering contributing to our project! 

Whether you're an experienced developer or just starting out, we welcome your contributions to help make our game even better.

## Reporting Issues

If you encounter any bugs or issues, please report them in the [Issue Tracker][#issue_tracker]. 

Include as much detail as possible, such as steps to reproduce the issue, expected vs. actual behavior, screenshots or logs.

## Merge Requests and Releases

When starting work on a new feature or bug fix, developers should always use the `main` branch as their starting point. 

> Learn how to [create a merge request when you work in a fork][#create_merge_requests_from_fork].

## Fork-based workflow

If you want to contribute changes without write access to the repository, you can still do so by forking. This is a simple way for anyone to contribute by creating your own copy of the project where you can make changes freely.

1. First, you'll need to fork the repository. 
2. Once you have a fork, clone it to your local machine and create a new branch.
3. Then, you can make few changes or even fix something.
4. After you've made your changes, you commit them locally and push these back to your fork on gitlab.
5. Finally, you can [create a merge request](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html#when-you-work-in-a-fork) from your fork's branch to upstream's `main` branch.

> We encourage you to [allow commits from upstream members](https://docs.gitlab.com/ee/user/project/merge_requests/allow_collaboration.html#allow-commits-from-upstream-members) so that they can [push to the fork as the upstream member](https://docs.gitlab.com/ee/user/project/merge_requests/allow_collaboration.html#push-to-the-fork-as-the-upstream-member) in order for you to reach out for help.

The merge request notifies project maintainers that you've made changes and allows them to review your code and potentially merge it into the `main` branch.

When your branch is merged, you may want to [update your fork](https://docs.gitlab.com/ee/user/project/repository/forking_workflow.html#update-your-fork) to be synced with upstream.

## Repository membership

Access to our [gitlab repository][#repo_url] is essential for collaborative development and ensuring the integrity of our project. If you're interested in contributing to our codebase or becoming a member, please follow these guidelines:

1. **Contribution**: Get involved in the project by contributing code, reporting issues, or participating in discussions on our [discord](https://discord.gg/tdmV3MxCn5) or [gitlab repository][#repo_url].
2. **Familiarization**: Read about development guidelines, coding style, conventions. Understanding these aspects will help you make meaningful contributions to our codebase.
3. **Request**: Upon a single merge request merged in `main`, anyone can request to become a member, reach out to other members for access.
4. **Review**: We will assess factors such as the quality of your contributions, adherence to our guidelines and alignment with our project's vision.
5. **Access**: Upon approval, a membership will be granted to our [gitlab repository][#repo_url], allowing you to directly contribute, review changes and collaborate with others.

While we welcome contributions from everyone, access to our repository is granted at our discretion.

We greatly value enthusiasm within our community and we can't wait to see what you'll bring to the table!

# Development Guidelines

## Code Style Guide

For consistency across the source code, we *must* follow the [Godot Engine Style Guide](https://docs.godotengine.org/en/stable/tutorials/best_practices/project_organization.html#style-guide) at any time:

>- Use **snake_case** for folder and file names (with the exception of C#
>  scripts). This sidesteps case sensitivity issues that can crop up after
>  exporting a project on Windows. C# scripts are an exception to this rule,
>  as the convention is to name them after the class name which should be
>  in PascalCase.
>- Use **PascalCase** for node names, as this matches built-in node casing.
>- In general, keep third-party resources in a top-level `addons/` folder, even
>  if they aren't editor plugins. This makes it easier to track which files are
>  third-party. There are some exceptions to this rule; for instance, if you use
>  third-party game assets for a character, it makes more sense to include them
>  within the same folder as the character scenes and scripts.

---

By following these guidelines, we aim to streamline our development process, maintain code quality, and ensure that our releases are stable and reliable.

Happy coding! 🎮✨

[#create_merge_requests_from_fork]: https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html#when-you-work-in-a-fork
[#create_merge_requests]: https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html
[#issue_tracker]: https://gitlab.com/open-fpsz/open-fpsz/-/issues
[#repo_url]:https://gitlab.com/open-fpsz/open-fpsz
