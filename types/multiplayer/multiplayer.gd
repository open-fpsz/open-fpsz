# This file is part of open-fpsz.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
## This class defines the multiplayer game type.
class_name Multiplayer extends Node

signal connected_to_server
signal connection_failed

## Enumeration that defines supported game modes.
enum Mode {
	## Free-for-all mode where players compete individually.
	FREE_FOR_ALL,
	## Rabbit mode where players chase and protect a designated "rabbit" player.
	RABBIT,
	## Capture the flag mode where teams compete to capture each other's flags.
	CAPTURE_THE_FLAG,
	## Arena mode where players engage in team deathmatch battles.
	ARENA,
	## Ball mode where teams aim to control the ball and score goals.
	BALL
}

## The scoreboard to keep track of scores.
@export var scoreboard : Scoreboard
## The multiplayer mode.
@export var mode : Mode = Mode.FREE_FOR_ALL
## The current map node.
@export var map : Node
## The maximum number of clients.
@export var MAX_CLIENTS := 24
## The time it takes for a player to respawn when killed, secconds (s).
@export var VICTIM_RESPAWN_TIME : float = 3.0
## The total duration of a match, in secconds (s).
@export var MATCH_DURATION := 1200
## The total duration of the warmup phase of a match.
@export var WARMUP_START_DURATION := 25

@export_group("Spawned Scenes")
@export var _PLAYER : PackedScene
@export var _FLAG : PackedScene

## This is the match timer.
var timer:Timer

## The [Teams] manager.
@onready var teams : Teams = $Teams
## The spawn root for [Player] nodes.
@onready var players : Node = $Players
## The spawn root for [Flag] nodes.
@onready var objectives : Node = $Objectives
## The spawn root for Map nodes.
@onready var map_root : Node = $Map
# The spawn root for the scoreboard node.
@onready var _scoreboard_spawn_root : Node = $Scoreboard
## The [Ping] manager.
@onready var ping : Ping = $Ping

func _unhandled_input(event : InputEvent) -> void:
	if event.is_action_pressed("exit"):
		if is_peer_connected():
			_leave_match.rpc_id(get_multiplayer_authority())

func is_peer_connected() -> bool:
	if not multiplayer.multiplayer_peer or multiplayer.multiplayer_peer is OfflineMultiplayerPeer:
		return false
	return multiplayer.multiplayer_peer.get_connection_status() == MultiplayerPeer.CONNECTION_CONNECTED

func set_map(new_map: Node) -> void:
	map = new_map
	MapsManager.current_map = map
	map_root.add_child(map)

## This method starts a server.
func start_server(port : int, map_scene : PackedScene, _mode: Mode = mode, username : String = "mercury") -> void:
	# setup enet server peer
	var peer : ENetMultiplayerPeer = ENetMultiplayerPeer.new()
	peer.create_server(port, MAX_CLIENTS)
	multiplayer.peer_connected.connect(_on_peer_connected)
	multiplayer.peer_disconnected.connect(_on_peer_disconnected)
	multiplayer.multiplayer_peer = peer
	
	timer = Timer.new()
	timer.one_shot = true
	add_child(timer)
	
	map = map_scene.instantiate()
	map_root.add_child(map)
	
	mode = _mode
	
	ping.synchronized.connect(scoreboard._on_ping_sync)
	
	match mode:
		Mode.RABBIT:
			var flag : Flag = _FLAG.instantiate()
			objectives.add_child(flag)
			var spawn : Node3D = map.get_objective_spawn()
			flag.global_position = spawn.global_position
			flag.respawn_timer.timeout.connect(func() -> void:
				if spawn:
					flag.waypoint.text = ""
					flag.global_position = spawn.global_position
					flag.state = flag.FlagState.ON_STAND
			)

			teams.team_added.connect(_on_team_added)
			teams.team_erased.connect(_on_team_erased)
			teams.add_teams(["rabbit", "chasers"])
			flag.grabbed.connect(
				func(carry:FlagCarryComponent) -> void:
					carry.owner.hud.objective_label.set_visible(true)
					switch_team(carry.owner.peer_id, "rabbit")
					flag.respawn_timer.stop()
			)
			flag.dropped.connect(
				func(carry:FlagCarryComponent) -> void:
					carry.owner.hud.objective_label.set_visible(false)
					switch_team(carry.owner.peer_id, "chasers")
					flag.respawn_timer.start()
			)
			
			_scoreboard_spawn_root.add_child(
				RabbitScoringComponent.new(scoreboard, flag))
			_scoreboard_spawn_root.add_child(
				DeathmatchScoringComponent.new(scoreboard, players))
			
			players.child_entered_tree.connect(func(_player:Player) -> void:
				teams["chasers"].add(_player.peer_id, _player.username)
				_player.damage.connect(_rabbit_damage_handler)
			)
			
		Mode.FREE_FOR_ALL:
			scoreboard.add_panel()
			_scoreboard_spawn_root.add_child(
				DeathmatchScoringComponent.new(scoreboard, players))
			players.child_entered_tree.connect(func(_player:Player) -> void:
				var panel : ScorePanel = scoreboard.get_panel(0)
				panel.add_entry(_player.peer_id, _player.username)
				_player.damage.connect(_ffa_damage_handler)
			)
	
	# when a new player is added into tree
	players.child_entered_tree.connect(func(_player:Player) -> void:
		# make sure we have enough players to start the match
		if players.get_child_count() > 1 and timer.is_stopped():
			_start_match()
	)
	
	print("mode `%s` loaded" % Mode.keys()[mode])
	
	if DisplayServer.get_name() != "headless":
		add_player(1, username)

func _rabbit_damage_handler(source: Node, target: Node, amount: float) -> void:
	assert(target.find_children("*", "Health"))
	if source == target or source.team_id != target.team_id:
		target.health.damage.rpc(amount, source.peer_id)

func _ffa_damage_handler(source: Node, target: Node, amount: float) -> void:
	assert(target.find_children("*", "Health"))
	target.health.damage.rpc(amount, source.peer_id)

func _on_peer_connected(peer_id: int) -> void:
	print("peer `%d` connected" % peer_id)

func _on_peer_disconnected(peer_id : int) -> void:
	print("peer `%d` disconnected" % peer_id)
	if players.get_child_count() < 2:
		# stop timer when there is not enough players
		timer.stop()
		var player: Player = players.get_child(0)
		if player:
			player.hud.timer_label.text = "Warmup"

func _start_match() -> void:
	scoreboard.scoring = true
	timer.start(WARMUP_START_DURATION) # wait few seconds
	await timer.timeout # wait for the starting countdown to finish
	for player: Player in players.get_children():
		if player.has_flag():
			var flag: Flag = player.flag_carry_component._flag
			player.flag_carry_component.drop(player.linear_velocity)
			var spawn : Node3D = map.get_objective_spawn()
			flag.global_position = spawn.global_position
	scoreboard.reset_scores()
	timer.start(MATCH_DURATION) # restart timer with match duration
	players.respawn() # respawn everyone
	if not timer.timeout.is_connected(_on_post_match):
		timer.timeout.connect(_on_post_match)

func _on_post_match() -> void:
	# disconnect handler for timer reuse
	if timer.timeout.is_connected(_on_post_match):
		timer.timeout.disconnect(_on_post_match)
	# @TODO: display end of match stats with scoreboard data
	scoreboard.reset_scores()
	scoreboard.scoring = false
	for player: Player in players.get_children():
		if player.has_flag():
			var flag: Flag = player.flag_carry_component._flag
			player.flag_carry_component.drop(player.linear_velocity)
			var spawn : Node3D = map.get_objective_spawn()
			flag.global_position = spawn.global_position
	# restart match
	_start_match()

func join_server(host : String, port : int, username : String) -> void:
	var peer : ENetMultiplayerPeer = ENetMultiplayerPeer.new()
	peer.create_client(host, port)
	multiplayer.connected_to_server.connect(_on_connected_to_server.bind(username))
	multiplayer.connection_failed.connect(_on_connection_failed)
	multiplayer.server_disconnected.connect(_on_server_disconnected)
	multiplayer.multiplayer_peer = peer
	if map_root.get_child_count() > 0:
		map = map_root.get_child(0)

func _on_server_disconnected() -> void:
	Game.exit_pressed.emit()
	queue_free()

func add_player(_peer_id : int, username : String) -> void:
	var player : Player = _PLAYER.instantiate()
	# @NOTE: MultiplayerSpawner needs valid names so we can use either
	# `add_child(node, true)` or `player.name = str(_peer_id)`.
	# > Unable to auto-spawn node with reserved name: @RigidBody3D@101.
	# > Make sure to add your replicated scenes via 'add_child(node, true)' 
	# > to produce valid names.
	player.name = str(_peer_id)
	player.peer_id = _peer_id
	player.username = username
	# @NOTE: player scene requires both params prior being added to the tree
	players.add_child(player)
	# @TODO: get spawn location randomly instead of using points
	player.global_position = Game.type.map.get_player_spawn().global_position
	player.killed.connect(_on_player_killed)

## This method switch a [param peer_id] to the specified team along with its score.
func switch_team(peer_id: int, to_team_name: String) -> void:
	var score : Vector3i = scoreboard.get_score(peer_id)
	teams.switch_team(peer_id, to_team_name)
	scoreboard.set_score(peer_id, score)

# This method is called when the [member Teams.team_addded] signal is emitted.
func _on_team_added(team_name: String) -> void:
	var panel : ScorePanel = scoreboard.add_panel(team_name)
	panel.title.show()
	var team : Team = teams[team_name]
	panel.title.set_text(team_name)
	team.renamed.connect(panel.title.set_text)
	team.player_added.connect(_on_team_player_added)
	team.player_erased.connect(_on_team_player_erased)

# This method is called when the [member Teams.team_erased] signal is emitted.
func _on_team_erased(team_name: String) -> void:
	var panel : ScorePanel = scoreboard.panels.get_node(team_name)
	scoreboard.remove_panel(panel)

# This method is called when the [member Team.player_added] signal is emitted.
func _on_team_player_added(team_name: String, peer_id: int, username : String = "newblood") -> void:
	var panel : ScorePanel = scoreboard.panels.get_node(team_name)
	panel.add_entry(peer_id, username)
	var player:Player = players.get_node(str(peer_id))
	player.team_id = teams[team_name].get_instance_id()

# This method is called when the [member Teams.Team.player_erased] signal is emitted.
func _on_team_player_erased(team_name: String, peer_id: int) -> void:
	var panel : ScorePanel = scoreboard.panels.get_node(team_name)
	panel.remove_entry_by_peer_id(peer_id)
	var player:Player = players.get_node(str(peer_id))
	player.team_id = -1

func _on_connected_to_server(username : String) -> void:
	connected_to_server.emit()
	_join_match.rpc_id(1, username)

func _on_connection_failed() -> void:
	connection_failed.emit()

func _on_player_killed(victim: Player, _killer:int) -> void:
	await get_tree().create_timer(VICTIM_RESPAWN_TIME).timeout
	var spawn: Node3D = Game.type.map.get_player_spawn()
	victim.respawn.rpc_id(1, spawn.global_position)

# This method notifies the server that a player wants to join the match. It 
# takes a single [param username] parameter and is invoked remotely by clients.
@rpc("any_peer", "call_remote", "reliable")
func _join_match(username : String) -> void:
	if multiplayer.is_server():
		# add player to server with unique id and username
		add_player(multiplayer.get_remote_sender_id(), username)

@rpc("any_peer", "call_local", "reliable")
func _leave_match() -> void:
	if multiplayer.is_server():
		var peer_id:int = multiplayer.get_remote_sender_id()
		var player : Player = players.get_node(str(peer_id))
		if player:
			var team : Team = teams.get_peer_team(peer_id)
			if team:
				team.erase(peer_id)
			players.remove_child(player)
			player.queue_free()
		_cleanup_peer.rpc_id(peer_id)

@rpc("authority", "call_local", "reliable")
func _cleanup_peer() -> void:
	multiplayer.multiplayer_peer = OfflineMultiplayerPeer.new()
	queue_free()

#func _exit_tree() -> void:
	# @NOTE: The `is_multiplayer_authority` method in `_extree` push an error 
	# about the multiplayer instance not being the currently active one.it_
	# see https://github.com/godotengine/godot/issues/77723
	#multiplayer.multiplayer_peer = OfflineMultiplayerPeer.new()
