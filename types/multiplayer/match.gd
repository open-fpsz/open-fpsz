# This file is part of open-fpsz.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
class_name Match extends Node

enum MatchState {
	READY,
	RUNNING,
	PAUSED,
	ENDED
}

signal entered_state(state: MatchState)
signal exiting_state(state: MatchState)

@export var state : MatchState = MatchState.READY:
	set = set_state

func set_state(new_state: MatchState) -> void:
	if state == new_state: return
	exiting_state.emit(state)
	state = new_state
	entered_state.emit(state)

func _ready() -> void:
	pass # Replace with function body.

func start() -> void:
	state = MatchState.RUNNING

func end() -> void:
	state = MatchState.ENDED
	
func pause() -> void:
	#state = MatchState.PAUSED
	push_warning("not implemented yet")
	
func is_ready() -> bool:
	return state == MatchState.READY
