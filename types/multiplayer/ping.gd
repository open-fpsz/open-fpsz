# This file is part of open-fpsz.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
## This defines a ping manager.
##
## When added to your scene and connect the [member synchronized] signal to your
## custom handler in order to receive ping updates on a regular basis (as 
## defined by [param ping_interval] and [param sync_interval]) for each peer.
## [codeblock]
## func _ready() -> void:
##     $Ping.synchronized.connect(_on_ping_sync)
## 
## func _on_ping_sync(state: Dictionary) -> void:
##     for peer_id in state:
##         print("peer `%s` latency is %d ms" % [peer_id, state[peer_id]])
## [/codeblock]
class_name Ping extends Node

## This class defines a ping information to keep track of ping results 
## and provide average ping.
class PingInfo:
	const SIZE := 21
	var samples := PackedInt32Array()
	var pos := 0

	func _init() -> void:
		samples.resize(SIZE)
		for i in range(SIZE):
			samples[i] = 0

	func get_average() -> int:
		var value := 0
		for sample in samples:
			value += sample
		@warning_ignore("integer_division")
		return value / SIZE if value > 0 else 0

	func set_next(value: int) -> void:
		samples[pos] = value
		pos = (pos + 1) % SIZE

## Emitted when the state is synchronized.
signal synchronized(state : Dictionary)

## Controls activation state of the ping feature.
@export var active: bool = false
## The time interval (in milliseconds) between ping requests sent by the server to measure client latency.
@export_range(0.,1.,.01) var ping_interval: float = .2 # ms
## The time interval (in milliseconds) between state synchronization updates sent to connected clients.
@export_range(0.,1.,.01) var sync_interval: float = 1. # ms
## The size of the ping history buffer used to store past ping times for each client.
@export var ping_history: int = 10:
	set = set_ping_history

# internal variables
var _clients := {}
var _state := {}
var _last_ping := 0
var _pings := []
var _last_sync := 0

func _init() -> void:
	clear_pings()

func _ready() -> void:
	multiplayer.peer_connected.connect(_add_peer)
	multiplayer.peer_disconnected.connect(_del_peer)

func _exit_tree() -> void:
	multiplayer.peer_connected.disconnect(_add_peer)
	multiplayer.peer_disconnected.disconnect(_del_peer)

# synchronize state only on server
func _process(_delta: float) -> void:
	if not active \
	or multiplayer.multiplayer_peer is OfflineMultiplayerPeer \
	or multiplayer.multiplayer_peer.get_connection_status() \
		!= MultiplayerPeer.CONNECTION_CONNECTED \
	or not multiplayer.is_server() \
	or not multiplayer.has_multiplayer_peer():
		return
	# sync state at regular intervals
	var now := Time.get_ticks_msec()
	if sync_interval * 1000 > 0 and now >= _last_sync + sync_interval * 1000:
		synchronize()
		_last_sync = now
	# check and update ping intervals
	if now >= _pings[_last_ping] + ping_interval * 1000:
		_last_ping = (_last_ping + 1) % ping_history
		_pings[_last_ping] = now
		_ping.rpc(now)

## Clears the ping history array.
func clear_pings() -> void:
	_last_ping = 0
	_pings.resize(ping_history)
	_pings.fill(0)

## Sets the size of the ping history buffer and clears existing ping data.
func set_ping_history(value: int) -> void:
	if value < 1:
		return
	ping_history = value
	clear_pings()

## Adds a new peer to the registry
func _add_peer(peer_id: int) -> void:
	_clients[peer_id] = PingInfo.new()

## Deletes a peer from the registry
func _del_peer(peer_id: int) -> void:
	_clients.erase(peer_id)

@rpc("unreliable")
func _ping(time: int) -> void:
	_pong.rpc_id(1, time)

@rpc("any_peer")
func _pong(time: int) -> void:
	if not multiplayer.is_server():
		return
	# get id of the peer sending the pong
	var peer_id: int = multiplayer.get_remote_sender_id()
	# check if peer exists in the registered clients dictionary
	if not _clients.has(peer_id):
		return
	# init variables
	var now := Time.get_ticks_msec()
	var last := (_last_ping + 1) % ping_history
	var found := ping_history * ping_interval * 1000
	# search related ping in history
	for i in range(ping_history):
		# check if current ping matches received pong
		if time == _pings[last]:
			found = _pings[last]
			break
		# move to next ping in history
		last = (last + 1) % ping_history
	# compute round-trip time (RTT) and update ping info for this peer
	_clients[peer_id].set_next((now - found) / 2)

## This function sends the current state of ping information to all connected peers
## and emits a signal indicating that the state has been synchronized.
func synchronize() -> void:
	# check if server is active and has multiplayer peers connected
	if not active or not multiplayer.has_multiplayer_peer() or not multiplayer.is_server():
		return
	# clear current state
	_state.clear()
	# iterate over registered clients
	for peer_id:int in _clients:
		# set client average ping state
		_state[peer_id] = _clients[peer_id].get_average()
	# send state to connected peers
	_synchronize.rpc(_state)
	# emit signal to indicate that synchronization has occured
	synchronized.emit(_state)

@rpc("call_local", "unreliable")
func _synchronize(state: Dictionary) -> void:
	_state = state
	synchronized.emit(_state)
