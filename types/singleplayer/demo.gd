# This file is part of open-fpsz.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
class_name Singleplayer extends Node

@onready var player : Player = $Player
@onready var flag : Flag = $Flag
@onready var map : Node = $Blaze

func _ready() -> void:
	MapsManager.current_map = map
	player.health.exhausted.connect(_on_player_dead)
	var spawn : Node = map.get_objective_spawn()
	flag.global_position = spawn.global_position

func _unhandled_input(event: InputEvent) -> void:
	if event.is_action_pressed("exit"):
		queue_free()

func _on_player_dead(_player : Player) -> void:
	var spawn : Node3D = map.get_player_spawn()
	player.respawn(spawn.global_position)
