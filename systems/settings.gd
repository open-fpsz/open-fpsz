# This file is part of open-fpsz.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
extends Node

var _file : ConfigFile = ConfigFile.new()
const path : String = "user://settings.cfg"

func _ready() -> void:
	# make sure we load most recent core settings
	self.reset()
	# merge core settings with user-defined settings
	var err : Error = _file.load(self.path)
	if err == ERR_FILE_CANT_OPEN:
		push_warning("settings not found, using defaults")
	# save state
	save()

func get_value(section: String, key: String, default: Variant = null) -> Variant:
	return _file.get_value(section, key, default)

func set_value(section: String, key: String, value: Variant) -> void:
	return _file.set_value(section, key, value)

func reset() -> void:
	_file.clear()
	# video settings
	_file.set_value("ui", "scale", 2)
	_file.set_value("video", "quality", 1.)
	_file.set_value("video", "filter", 0)
	_file.set_value("video", "fsr_sharpness", 0)
	_file.set_value("video", "window_mode", 2)
	_file.set_value("video", "vsync", 0)
	_file.set_value("video", "taa", 0)
	_file.set_value("video", "msaa", 0)
	_file.set_value("video", "fxaa", 0)
	_file.set_value("video", "fov", 90)
	# controls settings
	_file.set_value("controls", "mouse_sensitivity", .6)
	_file.set_value("controls", "inverted_y_axis", false)
	# quality settings
	_file.set_value("quality", "shadow_size", 3)
	_file.set_value("quality", "shadow_filter", 2)
	_file.set_value("quality", "mesh_lod", 2)
	# environment settings
	_file.set_value("environment", "sdfgi", 0)
	_file.set_value("environment", "glow", 0)
	_file.set_value("environment", "ssao", 0)
	_file.set_value("environment", "ssr", 0)
	_file.set_value("environment", "ssil", 0)
	_file.set_value("environment", "volumetric_fog", 0)

func save() -> void:
	_file.save(self.path)
