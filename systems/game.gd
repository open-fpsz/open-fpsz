# This file is part of open-fpsz.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
extends Node

signal type_changed(type : Node)
signal exit_pressed

## The default [Environment] for the game.
var environment := Environment.new()

## This is the type currently used by the game.
var type : Node:
	set(new_type):
		# clean up previous type
		if type != null:
			type.queue_free()
		# swap types
		if type != new_type:
			# keep reference to new type
			type = new_type
			if type != null:
				get_tree().current_scene.add_child(type)
			type_changed.emit(type)

func quit() -> void:
	get_tree().quit()

func _unhandled_input(event : InputEvent) -> void:
	if event.is_action_pressed("exit"):
		exit_pressed.emit()
	
	# switch window mode
	if event.is_action_pressed("window_mode"):
		if DisplayServer.window_get_mode() == DisplayServer.WINDOW_MODE_FULLSCREEN:
			DisplayServer.window_set_mode(DisplayServer.WINDOW_MODE_WINDOWED)
		else:
			DisplayServer.window_set_mode(DisplayServer.WINDOW_MODE_FULLSCREEN)
	
	# switch mouse mode
	if OS.is_debug_build() and Input.is_action_just_pressed("mouse_mode"):
		if Input.mouse_mode == Input.MOUSE_MODE_VISIBLE:
			Input.mouse_mode = Input.MOUSE_MODE_CAPTURED
		elif Input.mouse_mode == Input.MOUSE_MODE_CAPTURED:
			Input.mouse_mode = Input.MOUSE_MODE_VISIBLE
