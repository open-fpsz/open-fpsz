# This file is part of open-fpsz.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
class_name Vanguard extends Node

@export var spine_ik_target_attachment : Marker3D
@export var hand_attachment : BoneAttachment3D

@onready var animation_tree : AnimationTree = $AnimationTree
@onready var spine_ik : SkeletonIK3D = $Node/Skeleton3D/SpineIK
@onready var spine_ik_target : Node3D = $SpineIKTarget

enum GroundState { GROUND_STATE_GROUNDED, GROUND_STATE_MID_AIR, GROUND_STATE_DEAD }

func _ready() -> void:
	spine_ik.start()

func _process(_delta : float) -> void:
	spine_ik_target.global_transform = spine_ik_target_attachment.global_transform

func set_locomotion(locomotion : Vector2, ground_speed_factor : float) -> void:
	animation_tree.set("parameters/Locomotion/blend_position", locomotion)
	animation_tree.set("parameters/GroundSpeed/scale", ground_speed_factor)

func set_ground_state(ground_state : GroundState) -> void:
	var transition_name : String = "grounded"
	if ground_state == GroundState.GROUND_STATE_MID_AIR:
		transition_name = "mid_air"
	if ground_state == GroundState.GROUND_STATE_DEAD:
		transition_name = "dead"
	animation_tree.set("parameters/Transition/transition_request", transition_name)
