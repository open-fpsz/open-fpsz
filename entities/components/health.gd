# This file is part of open-fpsz.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
## This class defines a Health component for entities.
class_name Health extends Area3D

## An entity with this component can be either dead or alive.
enum HealthState { DEAD, ALIVE }

## Emitted when a peer damaged this component too much.
signal killed(by_peer_id:int)
## Emitted when value is exhausted.
signal exhausted()
## Emitted when the value is updated.
signal updated(new_value:int)

## Emitted when the health component is damaged.
signal damaged(source: Node, target: Node, amount: int)

@export var collider:CollisionShape3D
@export var max_value:int = 255
@export var value:int = 255:
	set = set_value
@export var state : HealthState = HealthState.ALIVE

func _ready() -> void:
	# only collide with the layer 3 named "Damage", disable monitoring completely
	collision_layer = 0b00000000_00000000_00000000_00000100 
	collision_mask = 0
	monitoring = false

func set_value(new_value:int) -> void:
	value = clampi(new_value, 0, max_value)
	updated.emit(value)

@rpc("authority", "call_local", "reliable")
func damage(amount:int, by_peer_id:int) -> void:
	value -= amount
	if value == 0 and state != HealthState.DEAD:
		state = HealthState.DEAD
		killed.emit(by_peer_id)
		exhausted.emit(owner)

@rpc("authority", "call_local", "reliable")
func heal(amount:int = max_value) -> void:
	# add strictly positive amount to value
	value += clampi(amount, 1, max_value)
	state = HealthState.ALIVE
