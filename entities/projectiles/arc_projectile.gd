# This file is part of open-fpsz.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
class_name ArcProjectile extends ExplosiveProjectile

var _g: float = ProjectSettings.get_setting("physics/3d/default_gravity") # in m/s²
var _gravity: Vector3 =  _g * ProjectSettings.get_setting("physics/3d/default_gravity_vector")

func _physics_process(delta : float) -> void:
	# compute motion
	var previous_global_position : Vector3 = global_position
	# apply gravity
	velocity += _gravity * delta
	# compute new position
	global_position += velocity * delta
	# handle collision
	if shape_cast:
		shape_cast.target_position = to_local(previous_global_position)
		if shape_cast.is_colliding():
			destroy(shape_cast.collision_result[0].point)
