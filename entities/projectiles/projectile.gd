# This file is part of open-fpsz.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
## This class defines a projectile.
class_name Projectile extends Node3D

# @NOTE: while it's the correct fundamental constant, we're really looking 
# for « terminal velocity » here instead because earth is not a vacuum
const _speed_of_light := 299792458 # in m/s

## The initial speed when launched, measured in meters per second (m/s or mps).
@export_range(0, _speed_of_light) var speed : float = 42
## The time before it automatically self-destructs (never by default), measured in seconds (s).
@export var lifespan : float = 0.
## A knockback impulse applied to bodies it collides with, in Newtons.
@export var knockback: int
## The base amount of damage.
@export var damage: int
## The [param source] that launched this projectile.
var source: Node

var deflectable:bool = false
var inheritance:Vector3 = Vector3.ZERO
var shape_cast : ShapeCast3D = null
var velocity:Vector3 = Vector3.ZERO

func _init() -> void:
	# do not inherit transform from parent
	top_level = true
	# ensure projectile has shape
	ready.connect(func() -> void:
		for child in get_children(): 
			if child is ShapeCast3D: 
				shape_cast = child
		assert(shape_cast)
		if lifespan > 0:
			get_tree().create_timer(lifespan).timeout.connect(destroy)
	)

func destroy(_location: Vector3 = global_position) -> void:
	queue_free()

func _physics_process(delta : float) -> void:
	# compute motion
	var previous_global_position : Vector3 = global_position
	# compute new position
	global_position += velocity * delta
	# handle collision
	if shape_cast:
		shape_cast.target_position = to_local(previous_global_position)
		if shape_cast.is_colliding():
			destroy(shape_cast.collision_result[0].point)
