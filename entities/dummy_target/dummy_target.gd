# This file is part of open-fpsz.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
class_name DummyTarget extends RigidBody3D

@export var respawn_time := 0.0 # seconds

@onready var collision_shape_3d: CollisionShape3D = $CollisionShape3D

var start_pos : Vector3

func _ready() -> void:
	start_pos = global_position
	$TargetMesh/AnimationPlayer.play("gunTwoHanded")

func spawn(_killer_id : int) -> void:
	hide()
	collision_shape_3d.disabled = true
	await get_tree().create_timer(respawn_time).timeout
	global_position = start_pos
	show()
	collision_shape_3d.disabled = false
