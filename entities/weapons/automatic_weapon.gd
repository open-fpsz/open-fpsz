# This file is part of open-fpsz.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
class_name AutomaticWeapon extends Weapon

var _auto_trigger := Timer.new()

func _init() -> void:
	super._init()
	_auto_trigger.timeout.connect(trigger)
	add_child(_auto_trigger)

func _on_primary(pressed: bool) -> void:
	if pressed:
		trigger()
		_auto_trigger.start(cooldown)
	else: 
		_auto_trigger.stop()
