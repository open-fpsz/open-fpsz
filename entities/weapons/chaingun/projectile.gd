# This file is part of open-fpsz.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
class_name ChainGunProjectile extends Projectile

func _physics_process(delta: float) -> void:
	var previous_global_position: Vector3 = global_position
	global_position += velocity * delta
	# handle collision
	if shape_cast:
		var local_global_position: Vector3 = to_local(previous_global_position)
		shape_cast.target_position = Vector3(
			local_global_position.x, -local_global_position.z, local_global_position.y)
		shape_cast.target_position = shape_cast.target_position
		if shape_cast.is_colliding():
			var collider: Node = shape_cast.get_collider(0)
			if collider is Player:
				collider.damage.emit(source, collider, damage)
			destroy(shape_cast.collision_result[0].point)
