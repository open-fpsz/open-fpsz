# This file is part of open-fpsz.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
class_name ChainGun extends AutomaticWeapon

@export var _PROJECTILE : PackedScene
## The factor of velocity from the owner of this [Weapon] inherited by this [Projectile].
@export_range(0., 1., .01) var inheritance : float = 1.

@export var anim_player : AnimationPlayer
## The angular deviation from ideal line of fire in degrees
@export var min_spread: float
## The maximum angle the spread will increase to during heatup.
@export var max_spread: float
## The amount of time it takes for the chaingun to spin up or spin down.
@export var spin_period: float
## The amount of time it takes for the chaingun to overheat.
@export var heat_period: float
#@export var heat_time: float
## The fraction of the [member heat_period] at which the gun can start firing again
@export var cooldown_threshold: float
## Multiplier for the speed when calculating cooldown/heatup
@export var speed_cooldown_factor: float
## The [Shader] responsible for rendering heat while firing.
@export var heat_shader: Shader
# heat flag
var overheated: bool

func _on_triggered() -> void:
	# play the fire animation
	anim_player.play("fire")
	# init projectile
	var projectile : ChainGunProjectile = _PROJECTILE.instantiate()
	projectile.velocity = projectile.speed * $Nozzle.global_basis.z.normalized() + owner.linear_velocity * inheritance
	projectile.source = owner
	owner.add_child(projectile)
	projectile.global_transform = $Nozzle.global_transform
	projectile.shape_cast.add_exception(owner)

func _on_visibility_changed() -> void:
	if self.visible:
		#anim_player.play("equip")
		#self.sounds.play("equip")
		pass
