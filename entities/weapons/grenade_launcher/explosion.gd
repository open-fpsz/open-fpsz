# This file is part of open-fpsz.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
class_name GrenadeLauncherProjectileExplosion extends Node3D

## The component responsible for explosion damages
@export var explosive_damage : ExplosiveDamage

## The source of explosion.
var source: Node:
	set = set_source

func set_source(new_source: Node) -> void:
	source = new_source
	explosive_damage.source = new_source

## This is increment by 1 when a particle emitter is finished
var _finished_count : int = 0

## Called when the node enters the scene tree for the first time.
func _ready() -> void:
	for child in $Particles.get_children():
		child.emitting = true
		child.finished.connect(_on_finished)
		
## This is called by children particle nodes to destruct our 
## [GrenadeLauncherProjectileExplosion] once all emitters are finished
func _on_finished() -> void:
	_finished_count += 1
	if _finished_count == $Particles.get_child_count():
		queue_free()

