# This file is part of open-fpsz.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
## This class defines a [Weapon].
class_name Weapon extends Node3D

## Emitted after [member Weapon.trigger] is called.
signal triggered

## Emitted when [member ammo] is changed (useful for [HUD] display).
signal ammo_changed(new_ammo: int)

# enum WeaponState { EQUIPPED, UNEQUIPPED }

## The ammunition count.
@export_range(0, 9223372036854775295) var ammo: int:
	set = set_ammo

@rpc("authority", "call_local", "reliable")
func set_ammo(new_ammo: int) -> void:
	ammo = new_ammo
	ammo_changed.emit(new_ammo)

## The maxmimum ammunition count.
@export_range(0, 9223372036854775295) var max_ammo: int
## The number of ammo used when [member triggered] is emitted.
@export_range(0, 9223372036854775295) var ammo_usage: int
## The cooldown until this [Weapon] can be triggered again.
@export var cooldown: float = 1.

## The internal timer to handle trigger cooldown
var _cooldown_timer: Timer

func _init() -> void:
	_cooldown_timer = Timer.new()
	_cooldown_timer.one_shot = true
	_cooldown_timer.wait_time = cooldown
	add_child(_cooldown_timer)

## This methods triggers the [Weapon].
func trigger() -> void:
	if not _cooldown_timer.is_stopped() or ammo < ammo_usage:
		return
	# deduct ammo_usage from ammo count
	ammo -= ammo_usage
	# start cooldown timer
	_cooldown_timer.start(cooldown)
	# emit triggered signal
	triggered.emit()

# primary action handler
func _on_primary(pressed: bool) -> void:
	if pressed: trigger()
