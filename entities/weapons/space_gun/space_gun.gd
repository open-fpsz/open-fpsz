# This file is part of open-fpsz.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
## This class defines a SpaceGun.
class_name SpaceGun extends Weapon

@export var _PROJECTILE : PackedScene
## The factor of velocity from the owner of this [Weapon] inherited by this [Projectile].
@export_range(0., 1., .01) var inheritance : float = 1.
@export var anim_player : AnimationPlayer

func _on_triggered() -> void:
	# play the fire animation
	anim_player.play("fire")
	var projectile : SpaceGunProjectile = _PROJECTILE.instantiate()
	projectile.velocity = projectile.speed * $Nozzle.global_basis.z.normalized() + owner.linear_velocity * inheritance
	projectile.source = owner
	owner.add_child(projectile)
	projectile.global_transform = $Nozzle.global_transform
	projectile.shape_cast.add_exception(owner)

func _on_visibility_changed() -> void:
	if self.visible:
		#anim_player.play("equip")
		#sounds.play("equip")
		pass
