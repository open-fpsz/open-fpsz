# This file is part of open-fpsz.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
class_name SpaceGunProjectileExplosion extends Node3D

@export var explosive_damage : ExplosiveDamage

var source: Node:
	set = set_source

func set_source(new_source: Node) -> void:
	source = new_source
	explosive_damage.source = new_source

@onready var particles : GPUParticles3D = $Fire

func _ready() -> void:
	top_level = true
	particles.emitting = true
	particles.finished.connect(func() -> void: queue_free())
	if source:
		explosive_damage.source = source
