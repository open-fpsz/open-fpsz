# This file is part of open-fpsz.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
class_name BootMenu extends CanvasLayer

signal start_demo

@export var multiplayer_panel : MultiplayerPanelContainer
@export var settings_panel : SettingsPanelContainer

func _ready() -> void:
	multiplayer_panel.menu_pressed.connect(_on_main_menu_pressed)
	settings_panel.closed.connect(_on_main_menu_pressed)

func _on_demo_pressed() -> void:
	start_demo.emit()

func _on_multiplayer_pressed() -> void:
	multiplayer_panel.hide()
	multiplayer_panel.tab_container.current_tab = 0
	settings_panel.hide()
	$MainPanelContainer.hide()
	multiplayer_panel.show()
	show()

func _on_settings_pressed() -> void:
	multiplayer_panel.hide()
	multiplayer_panel.tab_container.current_tab = 0
	settings_panel.show()
	$MainPanelContainer.hide()
	show()

func _on_quit_pressed() -> void:
	Game.quit()

func _on_main_menu_pressed() -> void:
	multiplayer_panel.hide()
	settings_panel.hide()
	multiplayer_panel.tab_container.current_tab = 0
	$MainPanelContainer.show()
	show()
