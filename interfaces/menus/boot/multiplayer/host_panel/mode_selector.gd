# This file is part of open-fpsz.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
class_name ModeSelector extends OptionButton

func format(enum_key: String) -> String:
	# split enum value by underscores
	var words : PackedStringArray = enum_key.split("_")
	# capitalize first letter of each word
	for i in range(words.size()):
		words[i] = words[i].capitalize()
	# join words with spaces
	return " ".join(words)

func _ready() -> void:
	for mode:String in Multiplayer.Mode.keys():
		add_item(format(mode))

	# @NOTE: ctf, arena and ball are not implemented yet
	set_item_disabled(2, true)
	set_item_disabled(3, true)
	set_item_disabled(4, true)
