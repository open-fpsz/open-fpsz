# This file is part of open-fpsz.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
class_name SettingsPanelContainer extends PanelContainer

signal closed

# Quality presets.

func _on_very_low_preset_pressed() -> void:
	%TAAOptionButton.select(0)
	%MSAAOptionButton.select(0)
	%FXAAOptionButton.select(0)
	%ShadowSizeOptionButton.select(0)
	%ShadowFilterOptionButton.select(0)
	%MeshLODOptionButton.select(0)
	%SDFGIOptionButton.select(0)
	%GlowOptionButton.select(0)
	%SSAOOptionButton.select(0)
	%SSReflectionsOptionButton.select(0)
	%SSILOptionButton.select(0)
	%VolumetricFogOptionButton.select(0)

func _on_low_preset_pressed() -> void:
	%TAAOptionButton.select(0)
	%MSAAOptionButton.select(0)
	%FXAAOptionButton.select(1)
	%ShadowSizeOptionButton.select(1)
	%ShadowFilterOptionButton.select(1)
	%MeshLODOptionButton.select(1)
	%SDFGIOptionButton.select(0)
	%GlowOptionButton.select(0)
	%SSAOOptionButton.select(0)
	%SSReflectionsOptionButton.select(0)
	%SSILOptionButton.select(0)
	%VolumetricFogOptionButton.select(0)

func _on_medium_preset_pressed() -> void:
	%TAAOptionButton.select(1)
	%MSAAOptionButton.select(0)
	%FXAAOptionButton.select(0)
	%ShadowSizeOptionButton.select(2)
	%ShadowFilterOptionButton.select(2)
	%MeshLODOptionButton.select(1)
	%SDFGIOptionButton.select(1)
	%GlowOptionButton.select(1)
	%SSAOOptionButton.select(1)
	%SSReflectionsOptionButton.select(1)
	%SSILOptionButton.select(0)
	%VolumetricFogOptionButton.select(1)

func _on_high_preset_pressed() -> void:
	%TAAOptionButton.select(1)
	%MSAAOptionButton.select(0)
	%FXAAOptionButton.select(0)
	%ShadowSizeOptionButton.select(3)
	%ShadowFilterOptionButton.select(3)
	%MeshLODOptionButton.select(2)
	%SDFGIOptionButton.select(1)
	%GlowOptionButton.select(2)
	%SSAOOptionButton.select(2)
	%SSReflectionsOptionButton.select(2)
	%SSILOptionButton.select(2)
	%VolumetricFogOptionButton.select(2)

func _on_ultra_preset_pressed() -> void:
	%TAAOptionButton.select(1)
	%MSAAOptionButton.select(1)
	%FXAAOptionButton.select(0)
	%ShadowSizeOptionButton.select(4)
	%ShadowFilterOptionButton.select(4)
	%MeshLODOptionButton.select(3)
	%SDFGIOptionButton.select(2)
	%GlowOptionButton.select(2)
	%SSAOOptionButton.select(3)
	%SSReflectionsOptionButton.select(3)
	%SSILOptionButton.select(3)
	%VolumetricFogOptionButton.select(2)

func _on_apply_pressed() -> void:
	Settings.save()

func _on_reset_pressed() -> void:
	Settings.reset()
	# @TODO: reset also settings user interface to render actual state

func _on_close_pressed() -> void:
	hide()
	closed.emit()

# Adjustment settings.

#func _on_brightness_slider_value_changed(value: float) -> void:
	# The slider value is clamped between 0.5 and 4.
	#world_environment.environment.set_adjustment_brightness(value)

#func _on_contrast_slider_value_changed(value: float) -> void:
	# The slider value is clamped between 0.5 and 4.
	#world_environment.environment.set_adjustment_contrast(value)

#func _on_saturation_slider_value_changed(value: float) -> void:
	# The slider value is clamped between 0.5 and 10.
	#world_environment.environment.set_adjustment_saturation(value)
