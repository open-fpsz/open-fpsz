# This file is part of open-fpsz.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
extends OptionButton

func _ready() -> void:
	select(Settings.get_value("quality", "mesh_lod"))

func _on_item_selected(index : int) -> void:
	Settings.set_value("quality", "mesh_lod", index)
	if index == 0: # Very Low
		get_viewport().mesh_lod_threshold = 8.0
	if index == 0: # Low
		get_viewport().mesh_lod_threshold = 4.0
	if index == 1: # Medium
		get_viewport().mesh_lod_threshold = 2.0
	if index == 2: # High (default)
		get_viewport().mesh_lod_threshold = 1.0
	if index == 3: # Ultra
		# Always use highest LODs to avoid any form of pop-in.
		get_viewport().mesh_lod_threshold = 0.0
