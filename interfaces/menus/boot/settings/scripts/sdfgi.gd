# This file is part of open-fpsz.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
extends OptionButton

func _ready() -> void:
	select(Settings.get_value("environment", "sdfgi"))

func _on_item_selected(index: int) -> void:
	Settings.set_value("environment", "sdfgi", index)
	# This is a setting that is attached to the environment.
	# If your game requires you to change the environment,
	# then be sure to run this function again to make the setting effective.
	if index == 0: # Disabled (default)
		Game.environment.sdfgi_enabled = false
	elif index == 1: # Low
		Game.environment.sdfgi_enabled = true
		RenderingServer.gi_set_use_half_resolution(true)
	elif index == 2: # High
		Game.environment.sdfgi_enabled = true
		RenderingServer.gi_set_use_half_resolution(false)
