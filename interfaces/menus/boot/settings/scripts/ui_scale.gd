# This file is part of open-fpsz.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
extends OptionButton

func _ready() -> void:
	self.selected = Settings.get_value("ui", "scale")
	self.item_selected.emit(self.selected)

func _on_item_selected(index: int) -> void:
	Settings.set_value("ui", "scale", clamp(index, 0, 2))
	# When the screen changes size, we need to update the 3D
	# viewport quality setting. If we don't do this, the viewport will take
	# the size from the main viewport.
	var new_size := Vector2(
		ProjectSettings.get_setting(&"display/window/size/viewport_width"),
		ProjectSettings.get_setting(&"display/window/size/viewport_height")
	)
	# compute new size
	if index == 0: # Smaller (66%)
		new_size *= 1.5
	elif index == 1: # Small (80%)
		new_size *= 1.25
	elif index == 2: # Medium (100%) (default)
		new_size *= 1.
	elif index == 3: # Large (133%)
		new_size *= .75
	elif index == 4: # Larger (200%)
		new_size *= .5
	# update scale
	get_tree().root.set_content_scale_size(new_size)
