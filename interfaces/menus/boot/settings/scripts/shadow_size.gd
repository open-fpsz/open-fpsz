# This file is part of open-fpsz.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
extends OptionButton

func _ready() -> void:
	select(Settings.get_value("quality", "shadow_size"))

func _on_item_selected(index : int) -> void:
	Settings.set_value("quality", "shadow_size", index)
	if index == 0: # Minimum
		RenderingServer.directional_shadow_atlas_set_size(512, true)
		# Adjust shadow bias according to shadow resolution.
		# Higher resultions can use a lower bias without suffering from shadow acne.
		#Settings.set_value("quality", "shadow_bias", 0.06)
		#directional_light.shadow_bias = Settings.get_value("quality", "shadow_bias")

		# Disable positional (omni/spot) light shadows entirely to further improve performance.
		# These often don't contribute as much to a scene compared to directional light shadows.
		get_viewport().positional_shadow_atlas_size = 0
	if index == 1: # Very Low
		RenderingServer.directional_shadow_atlas_set_size(1024, true)
		#Settings.set_value("quality", "shadow_bias", 0.04)
		#directional_light.shadow_bias = Settings.get_value("quality", "shadow_bias")
		get_viewport().positional_shadow_atlas_size = 1024
	if index == 2: # Low
		RenderingServer.directional_shadow_atlas_set_size(2048, true)
		#Settings.set_value("quality", "shadow_bias", 0.03)
		#directional_light.shadow_bias = Settings.get_value("quality", "shadow_bias")
		get_viewport().positional_shadow_atlas_size = 2048
	if index == 3: # Medium (default)
		RenderingServer.directional_shadow_atlas_set_size(4096, true)
		#Settings.set_value("quality", "shadow_bias", 0.02)
		#directional_light.shadow_bias = Settings.get_value("quality", "shadow_bias")
		get_viewport().positional_shadow_atlas_size = 4096
	if index == 4: # High
		RenderingServer.directional_shadow_atlas_set_size(8192, true)
		#Settings.set_value("quality", "shadow_bias", 0.01)
		#directional_light.shadow_bias = Settings.get_value("quality", "shadow_bias")
		get_viewport().positional_shadow_atlas_size = 8192
	if index == 5: # Ultra
		RenderingServer.directional_shadow_atlas_set_size(16384, true)
		#Settings.set_value("quality", "shadow_bias", 0.005)
		#directional_light.shadow_bias = Settings.get_value("quality", "shadow_bias")
		get_viewport().positional_shadow_atlas_size = 16384
