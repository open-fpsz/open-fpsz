# This file is part of open-fpsz.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
extends HBoxContainer

@export var min_value : float = 0.25
@export var max_value : float = 2
@export var step : float = .05

func _ready() -> void:
	var value : float = Settings.get_value("video", "quality")
	for control : Range in [$SpinBox, $Slider]:
		control.min_value = min_value
		control.max_value = max_value
		control.step = step
		control.value = value
	get_viewport().scaling_3d_scale = value

func _on_value_changed(new_value : float) -> void:
	Settings.set_value("video", "quality", new_value)
	get_viewport().scaling_3d_scale = new_value

func _on_spin_box_value_changed(new_value : float) -> void:
	_on_value_changed(new_value)
	$Slider.value = new_value
#
func _on_slider_value_changed(new_value : float) -> void:
	_on_value_changed(new_value)
	$SpinBox.value = new_value
