# This file is part of open-fpsz.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
extends OptionButton

func _ready() -> void:
	select(Settings.get_value("video", "taa"))

func _on_item_selected(index: int) -> void:
	Settings.set_value("video", "taa", index) 
	# Temporal antialiasing. Smooths out everything including specular aliasing, 
	# but can introduce ghosting artifacts and blurring in motion. 
	# Moderate performance cost.
	# @WARNING: https://github.com/TokisanGames/Terrain3D/issues/302
	# get_viewport().use_taa = index == 1
