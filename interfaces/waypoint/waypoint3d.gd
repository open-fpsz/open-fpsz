# This file is part of open-fpsz.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
@tool
class_name Waypoint3D extends Node3D

signal text_changed(new_text: String)

# If `true`, the waypoint sticks to the viewport's edges when moving off-screen.
@export var sticky : bool = true 

# If `true`, the waypoint fades as the camera get closer.
@export var fade : bool = true

# The waypoint's text.
@export var text : String:
	set(value):
		text = value
		text_changed.emit(value)

## The foreground color to modulate the waypoint color
@export var foreground_color : Color = Color.WHITE

@onready var label : Label3D = $Label3D
@onready var sprite : Sprite3D = $Sprite3D

# Called when the node enters the scene tree for the first time.
func _ready() -> void:
	text_changed.connect(label.set_text)
	
func _process(_delta : float) -> void:
	sprite.modulate = foreground_color
	label.modulate = foreground_color
	label.outline_modulate.a = foreground_color.a
