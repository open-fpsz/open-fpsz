# This file is part of open-fpsz.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
@tool
## An efficient progress bar in 3D using a quad mesh and a shader.
class_name ProgressBar3D extends MeshInstance3D

## Emitted when value has been changed.
signal value_changed(new_value : float)

const progress_bar_shader : VisualShader = preload(
	"res://interfaces/progress_bar/resources/visual_shader.res")

## The minimum [member value].
@export var min_value : float = 0.:
	set(new_value):
		min_value = new_value
		value = value

## The maximum [member value].
@export var max_value : float = 255.:
	set(new_value):
		max_value = new_value
		value = value

## The current value.
@export var value : float = 255.:
	set = set_value

func set_value(new_value : float) -> void:
	value = clampf(new_value, min_value, max_value)
	_update_shader_params()
	value_changed.emit(value)

## The size of the border.
@export_range(0., 1.) var border : float = .2:
	set(new_border):
		border = new_border
		_update_shader_params()

## The billboard mode, see [member BaseMaterial3D.BillboardMode] for more details.
@export_enum("Disabled", "Enabled", "Y-Billboard") var billboard : int = 2:
	set(new_billboard):
		billboard = new_billboard
		_update_shader_params()

## The fill color.
@export var fill : Color = Color.GREEN:
	set(value):
		fill = value
		_update_shader_params()

## The background color.
@export var background : Color = Color(0, 0, 0, 0.5):
	set(value):
		background = value
		_update_shader_params()

## Setup the [QuadMesh], [ShaderMaterial] and [VisualShader] 
## when entering the node tree
func _enter_tree() -> void:
	# set default mesh
	if not mesh:
		mesh = QuadMesh.new()
		mesh.size = Vector2(1, .1)
	
	# set default material
	if not mesh.material or not mesh.material is ShaderMaterial:
		# @NOTE: Per-instance uniforms allow for better shader reuse and are 
		# therefore faster, so they should be preferred over duplicating the 
		# ShaderMaterial when possible.
		mesh.material = ShaderMaterial.new()
	
	# set default shader
	mesh.material.shader = progress_bar_shader
	if not mesh.changed.is_connected(_update_shader_params):
		mesh.changed.connect(_update_shader_params)
	
	# update shader params once
	mesh.emit_changed()

## This method updates instance shader params
func _update_shader_params() -> void:
	set_instance_shader_parameter("background", background)
	set_instance_shader_parameter("billboard", billboard)
	set_instance_shader_parameter("border", border)
	set_instance_shader_parameter("fill", fill)
	set_instance_shader_parameter("progress", value / max_value)
	set_instance_shader_parameter("size", mesh.size)
