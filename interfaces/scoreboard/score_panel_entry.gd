# This file is part of open-fpsz.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
class_name ScorePanelEntry extends HBoxContainer

@export var scoring: bool = true

@export var _ping : int:
	set = set_ping
@export var _username : String:
	set = set_username 
@export var _score : Vector3i = Vector3i.ZERO:
	set = set_score

@export var ping : PingLabel
@export var username : Label
@export var offense : Label
@export var defense : Label
@export var style : Label
@export var score : Label

var peer_id : int

func get_total() -> int:
	return _score.x + _score.y + _score.z

func _to_string() -> String:
	return "<ScorePanelEntry(%s)#%s>" % [name, get_instance_id()]

# setters

func set_username(new_username : String) -> void:
	_username = new_username
	username.text = new_username

func set_ping(new_ping : int) -> void:
	_ping = new_ping
	ping.text = str(_ping)

func set_score(new_score : Vector3i) -> void:
	if scoring:
		_score = new_score
		offense.text = str(_score.x)
		defense.text = str(_score.y)
		style.text = str(_score.z)
		score.text = str(get_total())

# This is called when the Scoreboard.scoring_state_changed signal is emmitted
func _on_scoring_state_changed(new_scoring: bool) -> void:
	scoring = new_scoring
