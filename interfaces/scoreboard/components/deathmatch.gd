# This file is part of open-fpsz.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
class_name DeathmatchScoringComponent extends Node

@export var ON_KILL_SCORE := Vector3i(1,0,0)

signal add_score(peer_id:int, score:Vector3i)

## This method returns a new bound deathmatch scoring component to be added in tree.
func _init(scoreboard:Scoreboard, players: Node) -> void:
	add_score.connect(scoreboard.add_score)
	players.child_entered_tree.connect(register)
	players.child_exiting_tree.connect(unregister)

func register(player: Player) -> void:
	player.killed.connect(_on_player_killed)

func unregister(player: Player) -> void:
	player.killed.disconnect(_on_player_killed)
	
func _on_player_killed(victim: Player, killer:int) -> void:
	if victim.peer_id != killer:
		add_score.emit(killer, ON_KILL_SCORE)
