# This file is part of open-fpsz.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
## This defines a rabbit scoring component.
class_name RabbitScoringComponent extends Node

const ON_GRAB_SCORE := Vector3i(1,0,0)
const ON_HOLD_SCORE := Vector3i(1,0,0)
const HOLD_SCORING_TIMER:float = 10.0 # seconds

var timer:= Timer.new()

signal add_score(peer_id:int, score:Vector3i)

# @TODO: remove this variable by passing flag to signals `grabbed` and `dropped`
# this if fine for rabbit but will be required for CTF
var _flag: Flag

# This method initializes a new rabbit scoring component.
func _init(scoreboard:Scoreboard, flag:Flag) -> void:
	timer.timeout.connect(_on_timer_timeout.bind(flag))
	add_score.connect(scoreboard.add_score)
	flag.grabbed.connect(_on_flag_grabbed)
	flag.dropped.connect(_on_flag_dropped)
	_flag = flag

func _ready() -> void:
	add_child(timer)
	
func _on_flag_grabbed(carry: FlagCarryComponent) -> void:
	assert(_flag)
	# prevent chained grab scoring abuse
	if _flag and (not _flag.last_carrier or _flag.last_carrier.owner.peer_id != carry.owner.peer_id):
		# set new carrier for `_on_timer_timeout`
		_flag.last_carrier = carry
		add_score.emit(carry.owner.peer_id, ON_GRAB_SCORE)
	timer.start(HOLD_SCORING_TIMER)

func _on_flag_dropped(_carry: FlagCarryComponent) -> void:
	assert(_flag)
	timer.stop()

func _on_timer_timeout(flag:Flag) -> void:
	assert(_flag)
	add_score.emit(flag.last_carrier.owner.peer_id, ON_HOLD_SCORE)
