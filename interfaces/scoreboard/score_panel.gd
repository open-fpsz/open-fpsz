# This file is part of open-fpsz.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
## This defines a score panel to hold [ScorePanelEntry] instances.
class_name ScorePanel extends Panel

# The ScorePanelEntry packed scene to instantiate
@export var _SCORE_PANEL_ENTRY:PackedScene
## This is the panel title.
@export var title:Label
## This is the container for [ScorePanelEntry] child nodes.
@export var entries:VBoxContainer

# This is the iterator index cursor.
var _iter_cursor:int = 0

# This method is an iterator initializer.
func _iter_init(_arg:Variant) -> bool:
	_iter_cursor = 0 # reset
	return _iter_cursor < entries.get_child_count()

# This method checks if the iterator has a next value.
func _iter_next(_arg:Variant) -> bool:
	_iter_cursor += 1
	return _iter_cursor < entries.get_child_count()

# This method gets the next iterator value.
func _iter_get(_arg:Variant) -> ScorePanelEntry:
	return entries.get_child(_iter_cursor)

func _to_string() -> String:
	return "<ScorePanel#%s>" % get_instance_id()

## This method adds an entry to the panel.
func add_entry(peer_id:int, username:String, score:Vector3i = Vector3i.ZERO) -> ScorePanelEntry:
	var entry:ScorePanelEntry = _SCORE_PANEL_ENTRY.instantiate()
	entry.name = str(peer_id)
	entry.peer_id = peer_id
	entries.add_child(entry)
	entry.ping.text = "" # @TODO: get player ping
	entry._username = username
	entry._score = score
	return entry

## This method removes a [ScorePanelEntry] from the [ScorePanel].
func remove_entry(entry:ScorePanelEntry) -> void:
	entries.remove_child(entry)
	entry.free()

## This method returns a [ScorePanelEntry] from the [ScorePanel].
func get_entry(index:int) -> ScorePanelEntry:
	return entries.get_child(index)

## This method removes a [ScorePanelEntry] from the [ScorePanel] by [param name] 
## and returns a boolean to indicate removal state.
func remove_entry_by_name(entry_name:String) -> bool:
	var path := NodePath(entry_name)
	if entries.has_node(path):
		var entry:ScorePanelEntry = entries.get_node(path)
		remove_entry(entry)
		return true
	else:
		return false

func remove_entry_by_peer_id(peer_id:int) -> bool:
	return remove_entry_by_name(str(peer_id))

func size() -> int:
	return entries.get_child_count()
