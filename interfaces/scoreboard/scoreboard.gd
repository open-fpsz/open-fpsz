# This file is part of open-fpsz.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
## This defines a scoreboard to track players, scores and teams.
## It is a container for [ScorePanel] instances.
class_name Scoreboard extends Control

signal scoring_state_changed(new_scoring: bool)

# The score panel scene to spawn.
@export var _SCORE_PANEL: PackedScene
## This is the container for [ScorePanel] child nodes.
@export var panels: GridContainer
## This controls whether [Entry] scores can be updated or not.
@export var scoring: bool:
	set = set_scoring
	
func set_scoring(new_scoring: bool) -> void:
	scoring = new_scoring
	scoring_state_changed.emit(scoring)

func _unhandled_input(event: InputEvent) -> void:
	if event.is_action_pressed("scoreboard"):
		show()
	elif event.is_action_released("scoreboard"):
		hide()
		
func _ready() -> void:
	panels.child_entered_tree.connect(_on_score_panel_added)
	panels.child_exiting_tree.connect(_on_score_panel_removed)

func _on_score_panel_added(panel: ScorePanel) -> void:
	if not panel.entries.child_entered_tree.is_connected(_on_entry_added):
		panel.entries.child_entered_tree.connect(_on_entry_added)

func _on_score_panel_removed(panel: ScorePanel) -> void:
	if panel.entries.child_entered_tree.is_connected(_on_entry_added):
		panel.entries.child_entered_tree.disconnect(_on_entry_added)
		
func _on_entry_added(entry: ScorePanelEntry) -> void:
	if not scoring_state_changed.is_connected(entry._on_scoring_state_changed):
		scoring_state_changed.connect(entry._on_scoring_state_changed)
		
func _on_entry_removed(entry: ScorePanelEntry) -> void:
	if scoring_state_changed.is_connected(entry._on_scoring_state_changed):
		scoring_state_changed.disconnect(entry._on_scoring_state_changed)

func _on_ping_sync(state: Dictionary) -> void:
	for panel: ScorePanel in panels.get_children():
		for entry in panel:
			entry.ping._on_sync(state)

func _to_string() -> String:
	return "<Scoreboard#%s>" % get_instance_id()

## This method adds a [ScorePanel] to the [Scoreboard] and returns a reference to the added [ScorePanel].
func add_panel(panel_name : String = "") -> ScorePanel:
	var score_panel: ScorePanel = _SCORE_PANEL.instantiate()
	var score_panel_name := str(panel_name)
	if score_panel_name.is_valid_identifier():
		score_panel.name = score_panel_name
	panels.add_child(score_panel)
	panels.columns = 2 if panels.get_child_count() > 2 else panels.get_child_count()
	return score_panel

## This method removes a [ScorePanel] from the [Scoreboard].
func remove_panel(panel: ScorePanel) -> void:
	panels.remove_child(panel)
	panel.free()

## This method returns a [ScorePanel] from the [Scoreboard].
func get_panel(index: int) -> ScorePanel:
	return panels.get_child(index)

## This method removes an entry by its node name in the tree.
func remove_entry_by_name(entry_name: String) -> bool:
	for panel : ScorePanel in panels.get_children():
		if panel.remove_entry_by_name(entry_name):
			return true
	return false

func get_entry(peer_id: int) -> ScorePanelEntry:
	for panel : ScorePanel in panels.get_children():
		for entry in panel:
			if entry.peer_id == peer_id:
				return entry
	return null

## This method returns the score of the given [param peer_id].
func get_score(peer_id: int) -> Vector3i:
	var entry : ScorePanelEntry = get_entry(peer_id)
	return entry._score if entry else Vector3i.ZERO
	
## This method sets the score of the given [param peer_id].
func set_score(peer_id:int, score:Vector3i) -> bool:
	var entry : ScorePanelEntry = get_entry(peer_id)
	if entry:
		entry._score = score
		return true
	return false

## This method adds to the score of the given [param peer_id].
func add_score(peer_id:int, score:Vector3i) -> bool:
	var entry : ScorePanelEntry = get_entry(peer_id)
	if entry:
		entry._score += score
		return true
	return false

## This method resets the scores of all [Entry] nodes.
func reset_scores() -> void:
	for panel : ScorePanel in panels.get_children():
		for entry in panel:
			entry._score = Vector3.ZERO

## This method returns the number of [ScorePanel] children.
func size() -> int:
	return panels.get_child_count()
