# This file is part of open-fpsz.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
@tool
class_name IFF extends Node3D

signal health_changed(new_value : float)
signal fill_changed(color : Color)
signal background_changed(color : Color)
signal billboard_changed(new_billboard : int)
signal username_changed(new_username : String)
signal border_changed(new_border : float)

# If `true`, the waypoint fades as the camera get closer.
#@export var fade : bool = true

## The minimum [member value].
@export var min_value : float = 0.:
	set(new_value):
		min_value = new_value
		value = value

## The maximum [member value].
@export var max_value : float = 255.:
	set(new_value):
		max_value = new_value
		value = value

## Current value.
@export var value : float = 255.:
	set(new_value):
		value = clampf(new_value, min_value, max_value)
		health_changed.emit(value)

## The border for the progress bar.
@export_range(0., 1.) var border : float = .2:
	set(new_border):
		border = new_border
		border_changed.emit(border)

## The username to display on top of this indicator.
@export var username : String = "Username":
	set = set_username

func set_username(new_name : String) -> void:
		username = new_name
		username_changed.emit(username)

## The foreground color to use for this indicator.
@export var fill : Color = Color.WHITE:
	set(value):
		fill = value
		fill_changed.emit(fill)

## The background color to use for this indicator.
@export var background : Color = Color(0, 0, 0, 0.5):
	set(color):
		background = color
		background_changed.emit(color)

## The billboard mode to use. See [member BaseMaterial3D.BillboardMode] for possible values.
@export_enum("Disabled", "Enabled", "Y-Billboard") var billboard : int = 2:
	set(new_billboard):
		billboard = new_billboard
		billboard_changed.emit(billboard)

@onready var label : Label3D = $Username
@onready var chevron : Sprite3D = $Chevron
@onready var health_bar : ProgressBar3D = $ProgressBar3D

func _enter_tree() -> void:
	if not username_changed.is_connected(_on_username_changed):
		username_changed.connect(_on_username_changed)
	if not health_changed.is_connected(_on_health_changed):
		health_changed.connect(_on_health_changed)
	if not fill_changed.is_connected(_on_fill_changed):
		fill_changed.connect(_on_fill_changed)
	if not background_changed.is_connected(_on_background_changed):
		background_changed.connect(_on_background_changed)
	if not border_changed.is_connected(_on_border_changed):
		border_changed.connect(_on_border_changed)
	if not billboard_changed.is_connected(_on_billboard_changed):
		billboard_changed.connect(_on_billboard_changed)

func _on_billboard_changed(new_billboard : int) -> void:
	label.billboard = new_billboard as BaseMaterial3D.BillboardMode
	health_bar.billboard = new_billboard
	chevron.billboard = new_billboard as BaseMaterial3D.BillboardMode

func _on_border_changed(new_border : float) -> void:
	health_bar.border = new_border

func _on_username_changed(new_username : String) -> void:
	# The label's text can only be set once the node is ready.
	if is_inside_tree():
		label.text = new_username

func _on_background_changed(color : Color) -> void:
	label.outline_modulate = color
	health_bar.background = color

func _on_fill_changed(color : Color) -> void:
	label.modulate = color
	health_bar.fill = color
	chevron.modulate = color

func _on_health_changed(new_value : float) -> void:
	health_bar.value = new_value
