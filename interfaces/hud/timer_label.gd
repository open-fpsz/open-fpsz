# This file is part of open-fpsz.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
extends Label

var timer: Timer = null

func _ready() -> void:
	if Game.type is Multiplayer:
		timer = Game.type.timer
	else:
		visible = false
		set_process(false)

func _process(_delta: float) -> void:
	if timer and not timer.is_stopped() \
	and !(multiplayer.multiplayer_peer is OfflineMultiplayerPeer) \
	and multiplayer.is_server():
		var minutes: int = int(timer.time_left / 60)
		var seconds : int = int(fmod(timer.time_left, 60))
		text = "%d:%02d" % [minutes, seconds]
