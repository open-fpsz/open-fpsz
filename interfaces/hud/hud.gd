# This file is part of open-fpsz.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
class_name HUD extends CanvasLayer

@export var health_bar:ProgressBar
@export var energy_bar:ProgressBar
@export var timer_label:Label
@export var objective_label:Label
@export var debug_label:Label
@export var ammo_label:Label
@export var throw_progress:TextureProgressBar

@onready var player:Player = owner

func _ready() -> void:
	health_bar.set_value(player.health.max_value)
	energy_bar.set_value(player.energy_max)

func _on_ammo_changed(new_ammo: int) -> void:
	ammo_label.text = str(new_ammo)
